import ezFetch from 'ez-fetch'

const API_URL = 'http://localhost:8000/api/configuration/residency';

// const API_URL = 'http://10.80.90.173:7001/api/configuration/residency';

export default {
  getResidencia(){
    return ezFetch.get(API_URL)
  },
  getResidenciaView(id){
    return ezFetch.get(`${API_URL}/${id}`)
  },
  createResidencia(residency){
    return ezFetch.post(`${API_URL}/`, residency)
  },
  updateResidencia(id, residency){
    return ezFetch.put(`${API_URL}/${id}/`, residency)
  },
  deleteResidencia(id, residency){
    return ezFetch.delete(`${API_URL}/${id}/`)
  }
}
