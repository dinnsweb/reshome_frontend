
import API_URL from '@/common/API_URL'


const API = 'api/configuration/residency';
const API_RECIDENCIA = API_URL+API;

export default {
  getResidencia(){
    return ezFetch.get(API_RECIDENCIA)
  },
  getResidenciaView(id){
    return ezFetch.get(`${API_RECIDENCIA}/${id}`)
  },
  createResidencia(residency){
    return ezFetch.post(`${API_RECIDENCIA}/`, residency)
  },
  updateResidencia(id, residency){
    return ezFetch.put(`${API_RECIDENCIA}/${id}/`, residency)
  },
  deleteResidencia(id, residency){
    return ezFetch.delete(`${API_RECIDENCIA}/${id}/`)
  }
}
