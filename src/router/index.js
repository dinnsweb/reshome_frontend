import Vue from 'vue'
import Router from 'vue-router'
import Landing from '@/components/Landing'
import Residencia from '@/components/configuracion/Residencia'
import Create from '@/components/configuracion/Create'
import Edit from '@/components/configuracion/Edit'
import ResidenciaView from '@/components/configuracion/Views/ResidenciaView'
import ResidenciaForm from '@/components/configuracion/Forms/ResidenciaForm'
import ResidenciaCrud from '@/components/configuracion/Cruds/ResidenciaCrud'
import HabitacionCrud from '@/components/configuracion/Cruds/HabitacionCrud'
import Login from '@/components/Login'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/residencia',
      name: 'Residencia',
      component: Residencia
    },
    {
      path: '/residencia/:id',
      name: 'ResidenciaView',
      component: ResidenciaView
    },
    {
      path: '/residencia/new',
      name: 'Create',
      component: Create
    },

    {
      path: '/residencia/:id/edit',
      name: 'Edit',
      component: Edit
    },
    {
      path: '/residenciacrud',
      name: 'ResidenciaCrud',
      component: ResidenciaCrud
    },
    {
      path: '/habitacioncrud',
      name: 'HabitacionCrud',
      component: HabitacionCrud
    },
  ]
})
